package fr.amil.DemoApplicationTests;

import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository {
    public User findOne(Long id);
}
