package fr.amil.DemoApplicationTests;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplicationTestsApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplicationTestsApplication.class, args);
	}

}
