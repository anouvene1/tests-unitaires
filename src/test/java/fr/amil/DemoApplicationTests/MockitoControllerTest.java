package fr.amil.DemoApplicationTests;

// JUnit
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

// Mockito
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MockitoControllerTest {

    @InjectMocks
    private UserController userController; // Mock instance de UserController

    @Mock
    private UserRepository userRepository; // Mock instance de UserRepository

    @Before
    public void init() {
        // Initialiser les mocks avant de lancer les tests
        MockitoAnnotations.initMocks(this);
    }

//    @Test
//    public void testGetUserById() {
//        UserController userController = new UserController();
//        User user = userController.get(1L);
//        assertEquals(1l, user.getId().longValue());
//    }

    @Test
    public void testGetUserById() {
        User u = new User();
        u.setId(1l);

        // Faire appel à un mock de UserRepository
        when(userRepository.findOne(1l)).thenReturn(u);

        // Faire appel à un mock de UserController
        User user = userController.get(1L);

        // Tester la méthode findOne de UserRepository via son mock
        verify(userRepository).findOne(1l);

        // Tester l'assertion
        assertEquals(1l, user.getId().longValue());
    }
}
