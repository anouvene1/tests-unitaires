package fr.amil.DemoApplicationTests;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class JUnitControllerTest {

    @Test
    public void testHomeController() {
        HomeController homeController = new HomeController();
        String result = homeController.home();

        assertEquals(result, "Hello World!");
    }
}
